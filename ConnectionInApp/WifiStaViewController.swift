
import UIKit

class WifiStaViewController: UIViewController,StreamDelegate
{
  
    var inputStream :InputStream!
    var outputStream :OutputStream!
    var host:CFString?
    var port:Int?
    
    
    @IBOutlet weak var iPTextField: UITextField!
    @IBOutlet weak var portNumberTextField: UITextField!
    @IBOutlet weak var connectBTN: UIButton!
    //傳輸命令textfield
    @IBOutlet weak var sendMessageTextField: UITextField!
    @IBOutlet weak var sendButton: UIButton!
    //傳輸命令TextView
    @IBOutlet weak var readMessageTextView: UITextView!
    //傳輸命令回收命令TextView
    @IBOutlet weak var SeeMessageTextView: UITextView!
    
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        

       
    }
    
    
    
    
    @IBAction func connectBTNAction(_ sender: UIButton)
    {
        connect()
       
    }
        
    
    func connect()
    {
        
        var cfReadStream: Unmanaged<CFReadStream>?
        var cfWriteStream: Unmanaged<CFWriteStream>?
        
        
        let host = iPTextField.text! as CFString
        let port = UInt32(portNumberTextField.text!  )
       
        CFStreamCreatePairWithSocketToHost(kCFAllocatorDefault, host , port!, &cfReadStream, &cfWriteStream)
       
        inputStream = cfReadStream!.takeRetainedValue()
        outputStream = cfWriteStream!.takeRetainedValue()

        print("連線到\(host),ip\(String(describing: port))")
      
        
        //calling a delegate function
         
        inputStream?.delegate = self
        outputStream?.delegate = self
        
        
        inputStream?.schedule(in: RunLoop.current, forMode: RunLoop.Mode.default )
        outputStream?.schedule(in: RunLoop.current, forMode: RunLoop.Mode.default)
        
        if let  inputStream = inputStream ,let outputStream = outputStream
        {
          inputStream.open()
          outputStream.open()
            waitingData()
       
        }
    }
     
    
    func stream(_ aStream: Stream, handle eventCode: Stream.Event)
    {
        print("we are in delegate method")

        print("EventCode = \(eventCode)")

    
        switch (eventCode)
    {
            //case1 :
        case Stream.Event.openCompleted:
            if (aStream == outputStream)
            {
                print("output:OutPutStream opened")
            }
                print("Input = openCompleted")
            
            break
            
        case Stream.Event.endEncountered:
                if(aStream === outputStream)
                {
                    print("output:endEncountered\n")
                }
                print("Input = endEncountered\n")
            
                break

        case Stream.Event.hasSpaceAvailable:
            
                    if(aStream === outputStream)
                    {
                        print("output:hasSpaceAvailable\n")
                    }

                    print("Input = hasSpaceAvailable\n")
            
            
                    break

                case Stream.Event.hasBytesAvailable:
            
                    if(aStream === outputStream)
                        
                    {
                        print("output:hasBytesAvailable\n")
                    }
                    if aStream === inputStream
                    {
                        print("Input:hasBytesAvailable\n")

                        var buffer = [UInt8](repeating: 0, count: 4096)

                    }
                    
                default:
            print("default block")
    }

}
    
    func waitingData()
{
        guard let  inputStream = inputStream
        else
        {
            print("ERROR: input stream is nil")
            return
        }
        
        var buf = Array(repeating: UInt8(0), count: 12)
        DispatchQueue.global().async
        {
            while true
            {
                let n = inputStream.read(&buf, maxLength: buf.count)
                
                let data = Data(bytes: buf, count: n)
                
                DispatchQueue.main.async
                {
                    self.SeeMessageTextView.text =  String(data: data, encoding: .utf8)
                   
                    
                }
            }
        }
        
}
    
    func send (_ string:String)
{
        guard let outputStream = outputStream
          else
          {
              print("ERROR:output stream is nil")
            return
          }
        var buf = Array(repeating: UInt8(0), count: 1024)
        let data = string.data(using: .utf8)!
        data.copyBytes(to: &buf, count: data.count)
        outputStream.write(buf, maxLength: data.count)
}
    
    
    @IBAction func sendBTN(_ sender: UIButton)
{
        let string = sendMessageTextField.text ?? ""
    
        //let string =  "&GET,PI,W,s\r\n"
    
        if readMessageTextView.text.isEmpty
        {
            readMessageTextView.text = string
        }
        else
        {
            readMessageTextView.text = readMessageTextView.text + "\n" + string
          
        }
        do
        {
            send(string)
            
         
            
        }
   
}
    
    //disconnect
    func close ()
{
    
        self.inputStream.close()
        self.inputStream.remove(from: RunLoop.current, forMode: RunLoop.Mode.common)
        self.outputStream.close()
        self.outputStream.remove(from: RunLoop.current, forMode: RunLoop.Mode.common)
       
}
    
    
    @IBAction func stopConnectBTN(_ sender: Any)
{
        close()
}
    
    
    //收取鍵盤
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?)
{
        self.view.endEditing(true)
}
    
    
    
    
    
    
    
    
    
    
    
    

 

    
    
    
    




}
