

import UIKit
import CoreBluetooth

class BluetoothViewController: UIViewController , CBCentralManagerDelegate, CBPeripheralDelegate, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate
{
   
   //為了找出周圍的藍芽必須要使用CBCentralManager
   private var centralManger : CBCentralManager!
   
    
    //初始化
   func setUP()
    {
        centralManger = CBCentralManager.init(delegate: self, queue: nil)
        
    }
    
    // begin to scan
    func startScan()
    {
        print("開始找尋")
        centralManger?.scanForPeripherals(withServices: nil,options: [CBCentralManagerRestoredStateScanOptionsKey: true ])
    }
    
   
    // serch for bluetooth
    func centralManagerDidUpdateState(_ central: CBCentralManager)
    {
        switch central.state
        {
        case CBManagerState.poweredOn :
            print("藍芽開啟")
        case CBManagerState.unauthorized:
            print("沒有藍芽功能")
        case CBManagerState.poweredOff:
            print("藍芽關閉")
        default :
            print("未知狀態")


        }
        //判斷是否有已配對裝置，如果有就與他連線沒有就開始掃描裝置
        if isPaired()
        {
            centralManger.connect(cbPeripheral!, options: nil)
        }else
        {
        
        // serch for bluetooth
        central.scanForPeripherals(withServices: nil, options: nil)
        }
            
    }
    
    // stop scan
func stopBluetoothScan()
    {
        self.centralManger?.stopScan()
        print("Scanning stopped")
    }


    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        setUP()
        
       //tableview delegate /dataSource
         blueToothTableView.delegate = self
        blueToothTableView.dataSource = self
        // connecting label hidden
        connectedLabel.isHidden = true
        writeTextfield.delegate  = self
        
    }

 
    
    var PeripheralName :NSMutableArray = NSMutableArray()
    var PeripherUuids: NSMutableArray = NSMutableArray()
    private var peripherals: [ CBPeripheral] = []

     //called when central mangeral discovers a periphal with scanning
    func centralManager(_ central: CBCentralManager, didDiscover peripheral: CBPeripheral, advertisementData: [String : Any], rssi RSSI: NSNumber)
    {

//        print("pheripheral.name: \(String(describing: peripheral.name))")
//        print("advertisementData:\(advertisementData)")
//        print("RSSI: \(RSSI)")
//        print("peripheral.identifier.uuidString: \(peripheral.identifier.uuidString)\n")
    
        var name: NSString? = advertisementData["kCBAdvDataLocalName"] as? NSString
       if (name == nil)
        {
          name = "no name";
        }

        PeripheralName.add(name!)
       //print("\(PeripheralName)")
        PeripherUuids.add(peripheral.identifier.uuidString)
       // print("\(PeripherUuids)")
  
        blueToothTableView.reloadData()
        peripherals.append(peripheral)
        
        
        // 斷線處理
        // 儲存周邊端的UUID 重新連線需要這值
        let user = UserDefaults.standard
        user.set(peripheral.identifier.uuidString, forKey:"KEY_PERIPHERAL_UUID")
        user.synchronize()
        cbPeripheral = peripheral
        cbPeripheral?.delegate = self
        
       
        
        
        
        
        
        
        
      }
   
    //bluetooth TableView
    @IBOutlet weak var blueToothTableView: UITableView!
    
   
    //TableView Sections
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 1
    }
    
     // TableView cell
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {   //print("\(PeripherUuids)")
        return PeripherUuids.count
        
    }
    
    //TableView cell setting
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell =  tableView.dequeueReusableCell(withIdentifier:"Cell", for: indexPath) as! BlueToothListTableViewCell
   
        cell.BlueToothNameLabel.text = "\(PeripheralName[indexPath.row])"
        //print("\(PeripheralName)")
       return cell
        
    }
  
  //CBPeripheral
    var cbPeripheral: CBPeripheral? = nil
    //touch tableView to Connect bluetooth
    func  tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        for peripheral in peripherals {
                  if peripheral.name != nil  {
                      cbPeripheral = peripheral
                     centralManger?.stopScan()
                      break;
                  }
              }
              
              if cbPeripheral != nil {
                  centralManger!.connect(cbPeripheral!, options: nil)
                  connectedLabel.isHidden = false
                  connectedLabel.text = "Connecting"
              }
    }
    
    
    //connecting label
    @IBOutlet weak var connectedLabel: UILabel!
    //connect success message
    func centralManager(_ central: CBCentralManager, didConnect peripheral: CBPeripheral)
    {
     cbPeripheral?.delegate = self
    cbPeripheral?.discoverServices(nil)
        print("connect")
        
       
    }
    
 

    //connect failed massage
    func centralManager(_ central: CBCentralManager, didFailToConnect peripheral: CBPeripheral, error: Error?)
    {
        print("not connnect")
    }

    //serch for bluetooth service
    func peripheral(_ peripheral: CBPeripheral, didDiscoverServices error: Error?)
    {
        if error != nil
        {
            print("error discovering services ")
            return
        }
        guard let services = cbPeripheral?.services else
        {
            return
        }
        print("Discovered services:\(services)")
        for service in services
        {
           cbPeripheral?.discoverCharacteristics(nil, for: service)
        }

      
        
    }
    //save characteristic in array
    var charDictionary = [String:CBCharacteristic]()
    //find all characteristic sav in charDictionary
    func peripheral(_ peripheral: CBPeripheral, didDiscoverCharacteristicsFor service: CBService, error: Error?)
    {
        
                guard error == nil else
                {
                    print("ERROR:\(#function)")
                    print(error!.localizedDescription)
                    return
                }
        
                for characteristic in service.characteristics!
                {
                    let uuidString = characteristic.uuid.uuidString
                    charDictionary[uuidString] = characteristic
                    print("find:\(uuidString)")
                }
        

        
    }
    
 
    //如果沒收取到資料傳遞錯誤
    enum sendDataError :Error
    {
        case CharacteristNotFound
    }
    
    
    //send data to peripheral
    func sendData(_data:Data,uuidString:String,writeType:CBCharacteristicWriteType) throws
    {
        guard let characteristic = charDictionary[uuidString] else
        {
            throw sendDataError.CharacteristNotFound
        }
        cbPeripheral?.writeValue(_data, for: characteristic,type: writeType)
        
        
        
        
    }
    
    
    //if send data to peripheral be error
    func peripheral(_ peripheral: CBPeripheral, didWriteValueFor descriptor: CBDescriptor, error: Error?)
    {
        if error != nil
        {
            print("write data error : \(error!)")
        }
        print("successed")
    }
    
    //GATT
    let try_Characterisiic = "BEF8D6C9-9C21-4C9E-B632-BD58C1009F9F"
 
    
    //藍芽回報資訊textfieldView
    @IBOutlet weak var seeMessageTextView: UITextView!
    
    
    @IBAction func subscribeValue(_ sender: UISwitch)
    {
        cbPeripheral!.setNotifyValue(sender.isOn, for: charDictionary[try_Characterisiic]!)
    
    }
    
    
    
    
    
    
 //recevied data from peripheral
    func peripheral(_ peripheral: CBPeripheral, didUpdateValueFor characteristic: CBCharacteristic, error: Error?)
    {
        
       
        guard error == nil else
        {
         print("ERROR:\(#function)")
           print("error")
            return
        }
        
        if characteristic.uuid.uuidString == try_Characterisiic
        {
            
            let data = characteristic.value! as NSData
            let string = ">" + String(data: data as Data, encoding: .utf8)!
            print(string)
            
          
            

                if self.seeMessageTextView.text.isEmpty
                {
                    self.seeMessageTextView.text = string
                }
                else
                {
                    self.seeMessageTextView.text =   self.seeMessageTextView.text + "\n" + string

                    self.seeMessageTextView.text =  String(data: data as Data, encoding: .utf8)
                }
            }




       
        
    }
    //看資料
    @IBOutlet weak var readTextView: UITextView!
    //寫資料
    @IBOutlet weak var writeTextfield: UITextField!
    
    //傳遞命令按鈕
    @IBAction func sendClick(_ sender: UIButton)
    {
        let string = "&GET,PI,B,s\r\n"
        
       // let string =  writeTextfield.text ?? ""
        if readTextView.text.isEmpty
        {
            readTextView.text = string
        }
        else
        {
            readTextView.text = readTextView.text + "\n" + string
          
        }
        do
        {
          //  let stringWithRN = string + "\r\n"
            let stringWithRN = string
            let data = stringWithRN.data(using: .utf8)
          
            try sendData(_data: data!, uuidString: try_Characterisiic, writeType: .withResponse)
            
            print(stringWithRN)
            
        }
        catch
        {
            print(error)
        }
        
        
        
    }
    
    //讀取資料
    @IBAction func readDataClick(_ sender: UIButton)
    {
         let characteristic = charDictionary[try_Characterisiic]!
        cbPeripheral?.readValue(for: characteristic)
        
    }
    
    
    
    
    //收取鍵盤
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?)
    {
        self.view.endEditing(true)
    }
    
    //central重新執行,嘗試取回peripheral
    func isPaired() -> Bool
    {
        let user = UserDefaults.standard
        if let uuidString = user.string(forKey: "KEY_PERIPHERAL_UUID")
        {
            let uuid = UUID(uuidString: uuidString)
            let list = centralManger.retrievePeripherals(withIdentifiers: [uuid!])
            if list.count > 0
            {
                cbPeripheral = list.first!
                cbPeripheral?.delegate = self
                return true
            }
            
        }
        return false
    }
    
    //disconnect按鈕
    @IBAction func disConnectBTN(_ sender: UIButton)
    {
        centralManger.connect(cbPeripheral!, options: nil)
        unpair()
    }
    
    
    //斷線處理
    func centralManager(_ central: CBCentralManager, didDisconnectPeripheral peripheral: CBPeripheral, error: Error?)
    {
        connectedLabel.isHidden = true
        print("Disconnect")
        
        if isPaired()
        {
            centralManger.connect(cbPeripheral!, options: nil)
        }
    }
    
    //解除配對
    func unpair()
    {
        let user = UserDefaults.standard
        user.removeObject(forKey: "KEY_PERIPHERAL_UUID")
        user.synchronize()
        centralManger.cancelPeripheralConnection(cbPeripheral!)
    }
    
        
    
    
    
    
    
    
    
    
    
    
    
    
    
}
