
import Foundation
import Network

final class NetWorkMonitor
{
    
    static let  shared = NetWorkMonitor()
    private let queue =  DispatchQueue.global()
    private let montior : NWPathMonitor
    
    public private(set) var isConnected : Bool
    
}
